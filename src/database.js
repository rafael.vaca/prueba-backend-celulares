const moongose = require("mongoose");

const url = "mongodb://localhost:27021/pruebacelular"

moongose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    console.log('Conexión exitosa')
}).catch((error) => {
    console.log(error)
})
// Requerir los modulos
const express = require("express");
const cors = require("cors");
const logger = require("morgan");
const bodyParser = require("body-parser");
require("dotenv").config();

// Enlazar las ruta principal
const routes = require("./apis/routes");

// Config
const port = process.env.PORT || 3001;
const app = express();

// Middlewares
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use(logger("tiny"));

// DataBase
require("./database");

// Routers
app.use(routes);

app.listen(port, () => {
  console.log(`Server running on port: ${port}`);
});
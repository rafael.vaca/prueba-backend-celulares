const CelularModel = require("./models/celular");

const mostrar = async (req, res) => {
  await CelularModel.find()
    .then((celulares) => {
      if (celulares.length === 0) {
        return res.json({
          message: "No hay registros",
        });
      }
      return res.json(celulares);
    })
    .catch((error) => {
      console.log(error);
      return res.json(error);
    });
};

const mostrarPorId = async (req, res) => {
  const { celularId } = req.params;
  await CelularModel.findById(celularId)
    .then((celular) => {
      return celular === null
        ? res.json({
            message: `No se encontró un celular con el id: ${celularId}`,
          })
        : res.json(celular);
    })
    .catch((err) => {
      console.log(err);
      return res.json({
        message: "No se encontró el celular",
      });
    });
};

const crearCelulares = async (req, res) => {
  const { nombre, marca, precio, ram, espacio } = req.body;
  const celular = new CelularModel({ nombre, marca, precio, ram, espacio });
  await CelularModel.create(celular)
    .then(() => {
      return res.json({
        message: "Celular Creado",
      });
    })
    .catch((err) => {
      console.log(err);
      return res.json({
        err,
      });
    });
};

const actualizarCelulares = async (req, res) => {
  const { celularId } = req.params;
  const celular = req.body;
  await CelularModel.findByIdAndUpdate(celularId, celular, {
    new: true,
  })
    .then((celularActualizar) => {
      return res.json({
        message: "Celular Actualizado",
        celular: celularActualizar,
      });
    })
    .catch((err) => {
      console.log(err);
      return res.json({
        message: "Celular no encontrado!",
      });
    });
};

const eliminar = async (req, res) => {
  const { celularId } = req.params;
  await CelularModel.findByIdAndDelete(celularId)
    .then((celular) => {
      return celular === null
        ? res.json({
            message: `No se encontró un celular con id: ${celularId}`,
          })
        : res.json({
            message: `Celular eliminado correctamente`,
          });
    })
    .catch((err) => {
      console.log(err);
      return res.json({
        message: `No se pudo eliminar el celular`,
      });
    });
};

module.exports = {
  mostrar,
  crearCelulares,
  mostrarPorId,
  actualizarCelulares,
  eliminar,
};

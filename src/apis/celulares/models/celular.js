const moongonse = require("mongoose");
const celularSchema = moongonse.Schema(
    {
        nombre: String,
        marca: String,
        precio: Number,
        ram: Number,
        espacio: Number,
    },
    {
        timestamps: true,
        versionKey: false,
    }
)
const CelularModel = moongonse.model("celulares", celularSchema)
module.exports = CelularModel
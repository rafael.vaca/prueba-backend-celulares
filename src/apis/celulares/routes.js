const { Router } = require("express");

const { mostrar, crearCelulares, mostrarPorId, actualizarCelulares, eliminar } = require("./controller");
const celulares = Router();

celulares.get("/pruebacelular", mostrar);
celulares.post("/pruebacelular", crearCelulares);
celulares.get("/prueba/:celularId", mostrarPorId);
celulares.put("/prueba/:celularId", actualizarCelulares);
celulares.delete("/prueba-eliminar/:celularId", eliminar)

module.exports = celulares;
